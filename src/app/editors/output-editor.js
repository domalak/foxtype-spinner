angular.module('FoxTypeSpinner')
.component('outputEditor', {
  template: '<textarea id="outputEditor"></textarea>',
  bindings: {
    rewriteRes: '<'
  },
  controllerAs: 'oeCtrl',
  controller: function ($scope, Rewrite) {
    var oeCtrl = this;
    var textArea = document.getElementById('outputEditor');
    var editor = CodeMirror.fromTextArea(textArea, {
      lineWrapping: true
    });
    var doc = editor.getDoc();
    var chosenRewrites;

    $scope.$watch('oeCtrl.rewriteRes', function (value) {
      if (value) {
        chosenRewrites = [];
        value.sentences.forEach(function () {
          chosenRewrites.push(0);
        });

        fillDoc(value);
        markDoc(value);
        attachHints(value);
      }
    });

    function fillDoc(rewriteRes) {
      var outputText = Rewrite.getOutput(rewriteRes, chosenRewrites);
      doc.setValue(outputText);
    }

    function markDoc(rewriteRes) {
      var currentParagraph = 0;
      var currentTextSize = 0;

      rewriteRes.sentences.forEach(function (sentence, sid) {
        // if (sentence.nParagraph > currentParagraph) {
        //   currentParagraph = sentence.nParagraph;
        //   currentTextSize = 0;
        // }

        doc.markText({
          line: currentParagraph,
          ch: currentTextSize
        }, {
          line: currentParagraph,
          ch: sentence.rewrites[chosenRewrites[sid]].text.length
        }, {
          className: 'rewrite-' + sid
        });

        sentence.matches.forEach(function (match, mid) {
          var markStart = sentence.rewrites[chosenRewrites[sid]].text.indexOf(match.alternatives[chosenRewrites[sid]].text);
          if (markStart > -1) {
            var markEnd = markStart + match.alternatives[chosenRewrites[sid]].text.length;

            doc.markText({
              line: currentParagraph,
              ch: currentTextSize + markStart
            }, {
              line: currentParagraph,
              ch: currentTextSize + markEnd
            }, {
              css: 'color:red',
              className: 'alternative alternative-' + sid + '-' + mid
            });
          }

        });

        currentTextSize += sentence.rewrites[chosenRewrites[sid]].text.length + 1;
      })
    }

    function attachRewriteHint(rewrites, rewriteElements, paragraph, start, end, sid) {
      [].forEach.call(rewriteElements, function (el) {
        el.addEventListener('mouseenter', function listener () {
          var list = [];

          rewrites.forEach(function (rewrite, idx) {
            list.push({
              text: rewrite.text,
              hint: function (_a, _b, _data) {
                chosenRewrites[sid] = idx;

                fillDoc(oeCtrl.rewriteRes);
                markDoc(oeCtrl.rewriteRes);
                attachHints(oeCtrl.rewriteRes);
              }
            });
          })

          editor.showHint({
            hint: function () {
              return {
                list: list,
                from: {
                  line: paragraph,
                  ch: start
                },
                to: {
                  line: paragraph,
                  ch: end
                },
                selectedHint: chosenRewrites[sid]
              };
            },
            completeSingle: false
          });
        });
      })
    }

    function attachMatchHint(alternatives, alternativeElement, paragraph, start, end, selectedHint) {
      alternativeElement.addEventListener('mouseenter', function listener () {
        var list = [];

        alternatives.forEach(function (alternative, idx) {
          list.push({
            text: alternative.text,
            hint: function (_a, _b, data) {
              alternativeElement.innerHTML = data.text;
              alternativeElement.removeEventListener('mouseenter', listener);
              attachMatchHint(alternatives, alternativeElement, paragraph, start, end, idx);
            }
          });
        })

        editor.showHint({
          hint: function () {
            return {
              list: list,
              from: {
                line: paragraph,
                ch: start
              },
              to: {
                line: paragraph,
                ch: end
              },
              selectedHint: selectedHint
            };
          },
          completeSingle: false
        });
      });
    }

    function attachHints(rewriteRes) {
      var currentParagraph = 0;
      var currentTextSize = 0;

      rewriteRes.sentences.forEach(function (sentence, sid) {
        // if (sentence.nParagraph > currentParagraph) {
        //   currentParagraph = sentence.nParagraph;
        //   currentTextSize = 0;
        // }

        var rewriteElements = document.getElementsByClassName('rewrite-' + sid);

        attachRewriteHint(sentence.rewrites, rewriteElements, currentParagraph, currentTextSize, sentence.rewrites[chosenRewrites[sid]].text.length, sid);

        sentence.matches.forEach(function (match, mid) {
          var markStart = sentence.rewrites[0].text.indexOf(match.alternatives[0].text);
          var markEnd = markStart + match.alternatives[0].text.length;

          var alternativeElements = document.getElementsByClassName('alternative-' + sid + '-' + mid);
          var alternativeElement = alternativeElements.item(0);

          if (alternativeElement) {
            attachMatchHint(match.alternatives, alternativeElement, currentParagraph, currentTextSize + markStart, currentTextSize + markEnd, chosenRewrites[sid]);
          }
        });

        currentTextSize += sentence.rewrites[0].text.length + 1;
      })
    }
  }
});
