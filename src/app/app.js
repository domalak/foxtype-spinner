angular.module('FoxTypeSpinner', []);

function appController(Rewrite, Input) {
  var appCtrl = this;

  appCtrl.rewrite = function () {
    appCtrl.runDisabled = true;

    Rewrite.run(Input.get())
    .then(function (result) {
      appCtrl.rewriteRes = result.data;
    })
    .catch(function (err) {
      console.log(err);
    })
    .finally(function () {
      appCtrl.runDisabled = false;
    });
  }
}

angular.module('FoxTypeSpinner')
.component('spinnerApp', {
  templateUrl: 'app/app.html',
  // require: {
  //   ieCtrl: '?inputEditor'
  // },
  controllerAs: 'appCtrl',
  controller: appController
});
