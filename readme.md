First install npm and bower modules:
```
$ npm install
$ bower install
```

Then run the app:
```
npm run lite
```
