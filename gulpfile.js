var gulp = require('gulp');
var inject = require('gulp-inject');
var del = require('del');
var livereload = require('gulp-livereload');
var http = require('http');
var st = require('st');

var cssVendorFiles = [
  'node_modules/bootstrap/dist/css/bootstrap.min.css',
  'node_modules/codemirror/lib/codemirror.css',
  'node_modules/codemirror/addon/hint/show-hint.css'
];

var cssFiles = [
  'src/app/**/*.css'
];

var jsVendorFiles = [
  'node_modules/codemirror/lib/codemirror.js',
  'node_modules/codemirror/addon/hint/show-hint.js',
  'node_modules/angular/angular.min.js'
];

var jsFiles = [
  'src/app/**/*.js'
];

var assets = [
  'src/app/**/*.html'
];

gulp.task('clean', function(){
  return del(['build']);
});

gulp.task('copy-vendor-files', ['clean'], function () {
  var sources = cssVendorFiles
  .concat(jsVendorFiles);

  return gulp.src(sources, {
    base: 'node_modules'
  })
  .pipe(gulp.dest('build/node_modules'));
})

gulp.task('copy', ['copy-vendor-files'], function () {
  var sources = cssFiles
  .concat(jsFiles)
  .concat(assets);

  return gulp.src(sources, {
    base: 'src'
  })
  .pipe(gulp.dest('build'));
})

gulp.task('inject', ['copy'], function() {
  var target = gulp.src('src/index.html');
  var sources = gulp.src(cssVendorFiles.concat(cssFiles).concat(jsVendorFiles).concat(jsFiles), {
    read: false
  });

  return target.pipe(inject(sources, {
    relative: true,
    ignorePath: '..'
  }))
  .pipe(gulp.dest('build'))
  .pipe(livereload());
});

gulp.task('server', function(done) {
  http.createServer(
    st({ path: __dirname + '/build', index: 'index.html', cache: false })
  ).listen(3000, done);
});

gulp.task('default', ['inject', 'server'], function () {
  livereload.listen();
  return gulp.watch(cssFiles.concat(jsFiles).concat(assets).concat(['src/index.html']), [
    'inject'
  ]);
});
