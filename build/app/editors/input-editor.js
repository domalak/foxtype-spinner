angular.module('FoxTypeSpinner')
.component('inputEditor', {
  template: '<textarea id="inputEditor"></textarea>',
  bindings: {
    rewriteRes: '<'
  },
  controllerAs: 'ieCtrl',
  controller: function ($scope, Input) {
    // var ieCtrl = this;
    var doc = Input.getDoc();

    $scope.$watch('ieCtrl.rewriteRes', function (value) {
      if (value) {
        markDoc(value);
      }
    });

    function markDoc (rewriteRes) {
      var currentParagraph = 0;
      var currentTextSize = 0;
      rewriteRes.sentences.forEach(function (sentence) {
        // if (sentence.nParagraph > currentParagraph) {
          // currentParagraph = sentence.nParagraph;
          // currentTextSize = 0;
        // }

        sentence.matches.forEach(function (match) {
          var s = sentence.text.toLowerCase();
          var m = match.text.toLowerCase();
          var markStart = s.indexOf(m);
          var markEnd = markStart + match.text.length;
          doc.markText({
            line: currentParagraph,
            ch: currentTextSize + markStart
          }, {
            line: currentParagraph,
            ch: currentTextSize + markEnd
          }, {
            css: 'color:red'
          });
        })

        currentTextSize += sentence.text.length + 1;
      })
    }
  }
});
