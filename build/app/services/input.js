angular.module('FoxTypeSpinner')
.factory('Input', function () {
  var input = 'Hello, how are you?';

  var textArea = document.getElementById('inputEditor');
  var editor = CodeMirror.fromTextArea(textArea, {
    lineWrapping: true
  });
  var doc = editor.getDoc();
  doc.setValue(input);

  function get() {
    input = doc.getValue();
    return input;
  }

  function set(value) {
    input = value;
  }

  function getDoc() {
    return doc;
  }

  return {
    get: get,
    set: set,
    getDoc: getDoc
  };
})
