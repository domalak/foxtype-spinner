angular.module('FoxTypeSpinner')
.factory('Rewrite', function ($http) {
  function run(input) {
    return $http.get('https://api-generator.foxtype.com/spinner?texts=' + input);
  }

  function getOutputTextFromRes(rewriteRes, chosenRewrites) {
    // var currentParagraph = 0;
    return rewriteRes.sentences.reduce(function (outputText, sentence, idx) {
      // if (sentence.nParagraph > currentParagraph) {
      //   outputText += '\n';
      //   currentParagraph = sentence.nParagraph;
      // }

      return outputText + sentence.rewrites[chosenRewrites[idx]].text + ' ';
    }, '');
  }

  return {
    run: run,
    getOutput: getOutputTextFromRes
  };
})
